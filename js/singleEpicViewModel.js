var singleEpicViewModel = function(item) {
	var self = this;

	self.id = ko.observable(item.id);
	self.key = ko.observable(item.key);
	self.status = ko.observable(item.fields.status);
	self.summary = ko.observable(item.fields.summary);
	self.project = ko.observable(item.fields.project);
	self.fixVersions = ko.observable(item.fields.fixVersions);
	self.documentation = ko.observable(item.fields.customfield_10042);
	self.sprints = ko.observableArray(item.fields.customfield_10016);
	self.transitionTypes = ko.observableArray([self.status()]);

	self.getVersion = ko.computed(function() {
		if (self.fixVersions().length < 1) {
			return ([{"Name":"No Version"}]);
		} else {
			return ([{"Name":self.fixVersions()[0].name}]);
		}
	})

	self.activeSprint = ko.computed(function() {
		var name = new Array([]);
		var fields = new Array([]);
		var tempSprint = new Array([]);
		var tempSprint = ko.utils.arrayFilter(self.sprints(), function(sprint) {
			temp = sprint.split(',');
			return (temp[7].indexOf('completeDate=<null>') > -1)
		})
		if (tempSprint.length == 0) {
			return ("");
		} else {
			fields = tempSprint[0].split(',');
			//console.log(fields);
			name = fields[3].split('=');
			//console.log(name);
			return name[1];
		}
	})

	self.getDocumentation = ko.computed(function() {
		if (self.documentation() == null) {
			return ("");
		}
		if (self.documentation().value.toLowerCase().indexOf("not required") > -1) {
			return ("");
		}
		return (self.documentation().value);
	})
	self.timeEst = ko.computed(function() {
		if (item.fields.aggregatetimeestimate == null) {
			return (0);
		} else {
			return (Math.round(item.fields.aggregatetimeestimate / 3600));
		}
	})
	self.timeOrigEst = ko.computed(function() {
		if (item.fields.aggregatetimeoriginalestimate == null) {
			return (0);
		} else {
			return (Math.round(item.fields.aggregatetimeoriginalestimate / 3600));
		}
	});
	self.timeSpent = ko.computed(function() {
		if (item.fields.aggregatetimespent == null) {
			return (0);
		} else {
			return (Math.round(item.fields.aggregatetimespent / 3600));
		}
	})
	
	self.timeDelta = ko.computed(function() {
		if (self.timeSpent() == 0) {
			return (0);
		}
		var timeDelta = self.timeOrigEst() - (self.timeSpent() + self.timeEst());
		return (timeDelta);
	})

	self.timeLeft = ko.computed(function() {
		var timeLeft = self.timeOrigEst() - self.timeSpent();
		return (timeLeft);
	})
	self.updateAvailableTransitions = function() {
		self.transitionTypes([]);
		var uri = ViewModels.commonVM.getTransitions().replace("{issueIdOrKey}",self.key());
		var method = "GET";
		var request = {
			url: uri,
			type: method,
			success: function(data) {
				ko.utils.arrayForEach(data.transitions, function(status) {
					if (status.name !== self.status().name) {
						self.transitionTypes.push(status);
					}
				})
				//console.log(self.transitionTypes());
			}
		}
		$.ajax(request);

	}
	self.updateStatus = function(transition) {
		var uri = ViewModels.commonVM.getTransitions().replace("{issueIdOrKey}",self.key());
		var method = "POST";
		var data = JSON.stringify({
						"transition": {
							"id": transition.id
						}
					})
		self.status(transition);
		var request = {
			url: uri,
			type: method,
            contentType: 'application/json',
			data: data,
			success: function(response) {
				//console.log(response);
			},
			error: function(response) {
				console.log(response);
			}
		}
		$.ajax(request);
	}
	self.updateRelease = function(newVersion) {
        var uri = ViewModels.commonVM.baseIssueEditURI().replace("{issueIdOrKey}",self.key());
		var method = "POST";
		//console.log(self.fixVersions());
		
		var data = JSON.stringify({
			"update": {
			        "fixVersions" : [
			            {"add":
			                [
			                    {"name" : newVersion}
			                ]
			            }
			        ]
			    }
			})
		var request = {
			url: uri,
			type: method,
            contentType: 'application/json',
			data: data,
			success: function(response) {
				console.log(response);
			},
			error: function(response) {
				console.log(response);
			}
		}
		$.ajax(request);
	}

}