var commonVM = function() {

    var self = this;
    var baseURI = "https://dumacbusinesssystems.atlassian.net/";

    self.baseLinkURI = ko.observable(baseURI + "browse/");
    self.baseSearchURI = ko.observable(baseURI + "rest/api/2/search?jql=");
    self.baseIssueEditURI = ko.observable(baseURI + "rest/api/2/issue/{issueIdOrKey}");
    self.projectInfoURI = ko.observable(baseURI + "rest/api/2/project/"); // Add {projectIdOrKey}/versions 
    self.getBoardsURI = ko.observable(baseURI + "rest/agile/1.0/board");
    self.getSprintsURI = ko.observable(baseURI + "rest/agile/1.0/board/{boardId}/sprint");
    self.getEpicIssues = ko.observable(baseURI + "rest/agile/1.0/epic/"); //{epicIdOrKey}/issue)
    self.getEditMeta = ko.observable(baseURI + "/rest/api/2/issue/{issueIdOrKey}/editmeta");
    self.getProjectMeta = ko.observable(baseURI + "/rest/api/2/issue/createmeta?projectKeys={projectKey}&expand=projects.issuetypes.fields");
    self.getTransitions = ko.observable(baseURI + "/rest/api/2/issue/{issueIdOrKey}/transitions?expand=transitions.fields");

    self.fullLink = function(key) {
        return ko.computed(function() {
            var link = self.baseLinkURI() + key;
            return (link);
        })
    }

}