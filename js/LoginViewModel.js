var LoginViewModel = function(makeLoginViewVisible) {

    var self = this;

    self.isVisible = ko.observable(makeLoginViewVisible);
    self.msg = ko.observable();
    self.projects = ko.observableArray([""]);
    self.selectedProject = ko.observable();
    self.versions = ko.observableArray();
    self.sprints = ko.observableArray();
    self.boards = ko.observableArray();

    self.resetPicklists = function() {
        var values = ["ALL","EMPTY"];

        self.versions.removeAll();
        self.sprints.removeAll();

        values.forEach(function(item) {
            self.versions.push(item);
            self.sprints.push(item);
        })
    }

    self.projectChanged = function() {
        ViewModels.dataVM.epicArray.removeAll();
        ViewModels.dataVM.subTypes.removeAll();
        ViewModels.dataVM.statuses.removeAll();
        if (self.selectedProject() !== "") { //user changed
            //console.log("Updating version list");
        } else { // program changed
            //console.log("Nothing to update here");
            return;
        }
        self.pullBoards();
        self.pullVersions();
        self.pullSubTypes();
        self.pullStatuses();
    }

    self.pullStatuses = function() {
        var uri = ViewModels.commonVM.getProjectMeta().replace("{projectKey}",self.selectedProject());
        console.log(uri);
        var method = "GET"
        var request = {
            url: uri,
            type: method,
            success: function(data) {
                if (data.projects.length > 0) {
                    ko.utils.arrayForEach(data.projects[0].statuses, function(item) {
                        console.log(item);
                        ViewModels.dataVM.statuses.push(item);
                    })
                } else {
                    console.log("No projects returned")
                }
            },
            error: function(data) {
                console.log("Failed to get sub types");
            }
        }
        $.ajax(request);        
    }

    self.pullSubTypes = function() {
        var uri = ViewModels.commonVM.getProjectMeta().replace("{projectKey}",self.selectedProject());
        var method = "GET"
        var request = {
            url: uri,
            type: method,
            success: function(data) {
                if (data.projects.length > 0) {
                    ko.utils.arrayForEach(data.projects[0].issuetypes, function(item) {
                        console.log(item);
                        ViewModels.dataVM.subTypes.push(item);
                    })
                } else {
                    console.log("No projects returned")
                }
            },
            error: function(data) {
                console.log("Failed to get sub types");
            }
        }
        $.ajax(request);
    }

    self.pullVersions = function() {
        var uri = ViewModels.commonVM.projectInfoURI() + self.selectedProject() + "/versions";
        var method = "GET"
        var request = {
            url: uri,
            type: method,
            success: function(data) {
                self.resetPicklists()
                ko.utils.arrayForEach(data, function(item) {
                    self.versions.push(item.name);
                })
            },
            error: function(data) {
                console.log("Failed to get versions");
            }
        }
        $.ajax(request);
    }

    self.pullBoards = function() {
        self.boards.removeAll();
        var uri = ViewModels.commonVM.getBoardsURI();
        var method = "GET"
        var request = {
            url: uri,
            type: method,
            success: function(data) {
                ko.utils.arrayForEach(data.values, function(item) {
                    if (item.location.projectKey == ViewModels.loginVM.selectedProject()) {
                        self.boards.push(item);
                    }
                })
                //console.log(self.boards());
                self.pullSprints();
            },
            error: function(data) {
                console.log("Failed to get boards");
            }
        }
        $.ajax(request);
    }

    self.pullSprints = function() {
        ko.utils.arrayForEach(self.boards(), function(board) {
            if (board.type == "scrum") {
                self.getSprintBlock(board.id,0);
            }
        })
    }

    self.getSprintBlock = function(boardid, startat) {
        var uri = ViewModels.commonVM.getBoardsURI() + "/" + boardid + "/sprint?startat=" + startat
        var method = "GET"
        var request = {
            url: uri,
            type: method,
            success: function(data) {
                if (!data.isLast) {
                    self.getSprintBlock(boardid, (startat + data.maxResults));
                }
                ko.utils.arrayForEach(data.values, function(sprint) {
                    if (self.sprints.indexOf(sprint.name) < 0) {
                        //console.log(sprint);
                        self.sprints.push(sprint.name);
                    }
                })
            },
            error: function(data) {
                console.log("Failed to pull sprints");
            }
        }
        $.ajax(request);
    }

    self.login = function() {
        //console.log("logging in");
        var uri = ViewModels.commonVM.projectInfoURI();
        var method = "GET"
        var request = {
            url: uri,
            type: method,
            success: function(data) {
                //console.log(data);
                self.isVisible(false);
            	ViewModels.dataVM.isVisible(true);
                ko.utils.arrayForEach(data, function(item) {
                    //console.log(item);
                    self.projects.push(item.key);
                });
                self.msg();
            },
            error: function() {
            	self.isVisible(true);
            	ViewModels.dataVM.isVisible(false);
                self.msg("Please log into your JIRA account and try again");
            }
        };
        $.ajax(request);
    }

    self.filterVersions = ko.computed(function() {
        return ko.utils.arrayFilter(self.versions(), function(version) {
            return (version.toLowerCase() !== "all");
        })
    })

    self.test = ko.observable("test");
}
