
var ViewModels = {
    commonVM : new commonVM(),
    loginVM : new LoginViewModel(true),
    storyVM : new StoryViewModel(),
    dataVM : new ProjectViewModel(false)
}

ko.applyBindings(ViewModels);