var StoryViewModel = function() {
	var self = this;

    self.hoursClass = ko.observable("");

	self.loadStories= function() {
        var epicList = "";
        ko.utils.arrayForEach(ViewModels.dataVM.filterEpics(), function(epic){
            self.removeStory(epic.key());
            if (epicList.length == 0) {
                epicList += "'Epic Link'=" + epic.key();
            } else {
                epicList += " OR " + "'Epic Link'=" + epic.key();
            }
            if (epicList.length > 1000) {
                self.loadStory("",epicList,0);
                epicList = "";
            }
        })
        if (epicList.length > 0) {
            self.loadStory("",epicList,0);
        }
    }

    self.loadStory = function(epicKey, epicList, startAt) {
        if (startAt == 0 && ViewModels.dataVM.storyList().length > 0) {
            self.removeStory(epicKey);
        }
        if (epicList.length == 0) {
            var uri = ViewModels.commonVM.baseSearchURI() + "(" + "%22Epic%20Link%22=" + epicKey + ")"
        } else {
            var uri = ViewModels.commonVM.baseSearchURI() + "(" + epicList + ")"            
        }
        uri += "&fields=customfield_10013,issuetype"
            +  ",aggregatetimeestimate,aggregatetimespent,aggregatetimeoriginalestimate"
            +  ",assignee,fixVersions,status,summary,key,id,customfield_10016,customfield_10068"
        uri += "&startAt=" + startAt
        method = "GET"
        var request = {
            url: uri,
            type: method,
            success: function(data) {
                ko.utils.arrayForEach(data.issues, function(item) {
                    var thisStory = new singleStoryViewModel(item);
                    ViewModels.dataVM.storyList.push(thisStory);
//                    console.log(item);
                });
                ViewModels.dataVM.storyLoadingCount(ViewModels.dataVM.storyLoadingCount() - 1);
                if ((data.startAt + data.maxResults) < data.total) {
                    self.loadStory(epicKey, epicList, (data.startAt + data.maxResults));
                }
            },
            error: function(response) {
                ViewModels.dataVM.storyLoadingCount(ViewModels.dataVM.storyLoadingCount() - 1);
                console.log(response.responseText);
            }
        }
        ViewModels.dataVM.storyLoadingCount(ViewModels.dataVM.storyLoadingCount() + 1);
        $.ajax(request);
    }

    self.removeStory = function(parentEpicKey) {
        ViewModels.dataVM.storyList.remove(function(story) {
            return (story.epicKey() == parentEpicKey);
        });
    }

    self.getOverage = function(story) {
        var overageText = "";
        self.hoursClass("");
        if (story.fields.aggregateprogress.progress == 0) {
            return ((story.fields.aggregatetimeoriginalestimate / 3600) + " hrs");
        } 
        var overage = (story.fields.aggregatetimeoriginalestimate - (story.fields.aggregatetimeestimate + story.fields.aggregatetimespent)) / 3600;
        if (overage < 0) {
            overageText = "Over by " + Math.abs(overage) + " hrs";
            self.hoursClass("bg-danger text-white");
        } else {
            overageText = Math.abs(overage) + " hrs left";
        }
        return (overageText);
    }

    self.checkVersion = function(epicVersions,storyVersions) {
        if (storyVersions.length > 0 && epicVersions.length >  0) {
            var test = self.fixVersions().forEach(function(version) {
                result = epicVersions.some(function (el) {
                    return el.id === version.id;
                });
                if (result) return(result);
            })
        }
        return (result);
    }

    self.editField = function(issueKey, fieldName, newValue) {
        var updateURI = ViewModels.commonVM.baseIssueEditURI().replace("{issueIdOrKey}", issueKey);

        var data = {
                   "update" : {
                           "fixVersions" : [{"add" : {"name" : newValue[0].name}}]
                       }
                    }
        method = "PUT"
        //console.log(updateURI);
        var request = {
            url: updateURI,
            type: method,
            contentType: "application/json",
            data: data,
            success: function(data) {
                //console.log(updateURI);
                //console.log(data);
            },
            error: function(response) {
                console.log(data);
                console.log(response);
            }
        }
        $.ajax(request);
    }

    self.getInfo = function(issueKey) {
        var uri = ViewModels.commonVM.getEditMeta().replace("{issueIdOrKey}", issueKey);
        var method = "GET";
        var request = {
            url: uri,
            type: method,
            success: function(data) {
                //console.log(data);
            },
            error: function(response) {
                console.log(response);
            }
        }
        $.ajax(request)
    }
}
