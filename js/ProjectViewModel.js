var ProjectViewModel = function(makeProjectViewVisible) {

    var self = this;

    self.isVisible = ko.observable(makeProjectViewVisible);
    self.epicArray = ko.observableArray();
    self.storyList = ko.observableArray();
    self.statusSelect = ko.observableArray([""]);
    self.lastUpdateSelect = ko.observableArray(["","-1d","-4d","-1w","-4w","-90d"]);
    self.selectedStatus = ko.observable("");
    self.selectedSprint = ko.observable("");
    self.storyLoadingCount = ko.observable(0);

    // JIRA filters
    self.fixVersion = ko.observable("");
    self.lastUpdate = ko.observable("");

    // Epic search and filter
    self.searchEpic = ko.observable("");
    self.showPatches = ko.observable(true);
    self.DocsReqd = ko.observable(false);

    // Subtype search and filter
    self.subTypes = ko.observableArray();
    self.subTypeSelected = ko.observable();

    // Status search and filter
    self.statuses = ko.observableArray();
    self.statusSelected = ko.observable();

    self.grabDataBtnTxt = ko.computed(function() {
        if (self.storyLoadingCount() > 0) {
            return (self.storyLoadingCount() + " remaining...");
        } else {
            return ("Grab Data");
        }
    })

	self.loadGenType = function(startAt) {
        self.epicArray.removeAll();
        self.storyList.removeAll();
        self.statusSelect.removeAll();
        self.statusSelect.push("");
        if (self.subTypeSelected().name.toLowerCase().indexOf('epic') > -1) {
        	self.loadEpics(startAt);
        } else {
        	self.loadEpicsBySub(startAt);
        }
	}

    self.loadFresh = function(startAt) {
        self.epicArray.removeAll();
        self.storyList.removeAll();
        self.statusSelect.removeAll();
        self.statusSelect.push("");
        self.loadEpics(startAt);        
    }

    self.loadEpicsBySub = function(startAt) {
    	var uri = ViewModels.commonVM.baseSearchURI() + "issueType='" + self.subTypeSelected().name + "'";
        if (ViewModels.loginVM.selectedProject() != '') {
            uri += "+AND+project=%22" + ViewModels.loginVM.selectedProject() + "%22";
        }
        switch(self.statusSelected()) {
            case "ALL":
                // Add no status to the filter URI
                break;
            default:
                uri += "+AND+status=%22" + self.statusSelected() + "%22";            
        }
        switch(self.fixVersion()) {
            case "ALL":
                // Add no fix version to the filter URI
                break;
            case "EMPTY":
                uri += "+AND+fixVersion=EMPTY";
                break;
            default:
                uri += "+AND+fixVersion=%22" + self.fixVersion() + "%22";
        }
        switch(self.selectedSprint()) {
            case "ALL":
                // Add no sprint to the filter URI
                break;
            case "EMPTY":
                uri += "+AND+sprint=EMPTY";
                break;
            default:
                uri += "+AND+sprint=%22" + self.selectedSprint() + "%22";
        }
        switch(self.lastUpdate()) {
            case "":
                // Add no last update statement
                break;
            default:
                uri += "+AND+updated>=%22" + self.lastUpdate() + "%22";
        }
        uri += "&fields=customfield_10013";
        uri += "&startAt=" + startAt;
        var method = "GET"
        var request = {
            url: uri,
            type: method,
            success: function(data) {
            	console.log(data);
            	var epicLinkList = "";
                ko.utils.arrayForEach(data.issues, function(item){
                	epicLinkList += "'" + item.fields.customfield_10013 + "',";
                });
                epicLinkList = epicLinkList.slice(0, -1);
                self.loadEpicsFromList(epicLinkList, 0);
                if ((data.startAt + data.maxResults) < data.total) {
                    self.loadEpicsBySub((data.startAt + data.maxResults));
                }
            },
            error: function() {
                console.log("Failed to load sub types epic list");
            }
        };
		$.ajax(request);
    }

    self.loadEpicsFromList = function(epicLinkList, startAt) {
        var uri = ViewModels.commonVM.baseSearchURI() + "issueType=epic";
        uri += "+AND+key+in+(" + epicLinkList + ")";

        uri += "&fields=key,status,summary,fixVersions,project"
	        +   ",aggregatetimespent,aggregatetimeoriginalestimate,aggregatetimeestimate"
	        +   ",customfield_10042,customfield_10016";

        uri += "&startAt=" + startAt;
        var method = "GET"
        var request = {
            url: uri,
            type: method,
            success: function(data) {
                ko.utils.arrayForEach(data.issues, function(item){
                    var thisEpic = new singleEpicViewModel(item);
                    self.epicArray.push(thisEpic);
                    if (self.statusSelect.indexOf(item.fields.status.name) < 0) {
                        self.statusSelect.push(item.fields.status.name);
                    }
                });
                if ((data.startAt + data.maxResults) < data.total) {
                    self.loadEpicsFromList(epicLinkList,(data.startAt + data.maxResults));
                }
            },
            error: function() {
                console.log("Failed to load epics data from epic list");
            }
        };
        $.ajax(request);
    }

    self.loadEpics = function(startAt) {
        var uri = ViewModels.commonVM.baseSearchURI() + "issueType=epic";
        if (ViewModels.loginVM.selectedProject() != '') {
            uri += "+AND+project=%22" + ViewModels.loginVM.selectedProject() + "%22";
        }
        switch(self.statusSelected()) {
            case "ALL":
                // Add no status to the filter URI
                break;
            default:
                uri += "+AND+status=%22" + self.statusSelected() + "%22";            
        }
        switch(self.fixVersion()) {
            case "ALL":
                // Add no fix version to the filter URI
                break;
            case "EMPTY":
                uri += "+AND+fixVersion=EMPTY";
                break;
            default:
                uri += "+AND+fixVersion=%22" + self.fixVersion() + "%22";
        }
        switch(self.selectedSprint()) {
            case "ALL":
                // Add no sprint to the filter URI
                break;
            case "EMPTY":
                uri += "+AND+sprint=EMPTY";
                break;
            default:
                uri += "+AND+sprint=%22" + self.selectedSprint() + "%22";
        }
        switch(self.lastUpdate()) {
            case "":
                // Add no last update statement
                break;
            default:
                uri += "+AND+updated>=%22" + self.lastUpdate() + "%22";
        }
        uri += "+ORDER+BY+status";
        
        uri += "&fields=key,status,summary,fixVersions,project"
            +   ",aggregatetimespent,aggregatetimeoriginalestimate,aggregatetimeestimate"
            +   ",customfield_10042,customfield_10016";

        uri += "&startAt=" + startAt;
        //console.log(uri);
        var method = "GET"
        var request = {
            url: uri,
            type: method,
            success: function(data) {
                ko.utils.arrayForEach(data.issues, function(item){
                    //console.log(item);
                    var thisEpic = new singleEpicViewModel(item);
                    self.epicArray.push(thisEpic);
                    //self.epicArray.push(item);
                    if (self.statusSelect.indexOf(item.fields.status.name) < 0) {
                        self.statusSelect.push(item.fields.status.name);
                    }
                });
                if ((data.startAt + data.maxResults) < data.total) {
                    self.loadEpics((data.startAt + data.maxResults));
                }
            },
            error: function() {
                console.log("Failed to load epics data");
            }
        };
        var output = $.ajax(request);
        return output;
    }

    self.filterEpics = function() {
        return ko.utils.arrayFilter(self.epicArray(), function(epic) {
            return (((epic.summary().toLowerCase().indexOf(self.searchEpic().toLowerCase()) > -1) || (epic.key().toLowerCase().indexOf(self.searchEpic().toLowerCase()) > -1) || (epic.summary().length <= 3)) && ((epic.status().name == self.selectedStatus()) || (self.selectedStatus() == "")));
        })
    }

    self.filterStoryList = function(epicKey, types) {
        return ko.computed(function() {
            return ko.utils.arrayFilter(self.storyList(), function(story) {
                return ((story.epicKey() === epicKey()) && (types.indexOf(story.issueType()) > -1 ));
            })
        })
    }

    self.getNeedsStory = function(epic) {
        if (!epic.fields.customfield_10042) {
            return ("");
        }
        if (epic.fields.customfield_10042.value.toLowerCase().indexOf("not required") > -1 ) {
            return ("");
        }
        return (epic.fields.customfield_10042.value);
    }

    self.addStory = function(projectID, summary, epicLink, storyType) {
        var method = "POST";
        var thisSummary = "";
        switch(storyType.toLowerCase()) {
            case "dev story":
                thisSummary = 'DEV - ' + summary;
                break;
            case "patch":
                thisSummary = 'PATCH - ' + summary;
                break;
            case "qa story":
                thisSummary = 'QA - ' + summary;
                break;
            case "doc story":
                thisSummary = 'DOC - ' + summary;
                break;
            default:
                thisSummary = summary;
        }
        if ((ViewModels.dataVM.fixVersion().toLowerCase().indexOf("empty") > -1) || (ViewModels.dataVM.fixVersion().toLowerCase().indexOf("all") > -1)) {
            var data = JSON.stringify({"fields":{"project":{"id":projectID},
                                    "issuetype":{"name":storyType},
                                    "summary":thisSummary,
                                    "description":"Auto generated",
                                    "customfield_10013":epicLink
                                    }
                        })
        } else {
            var data = JSON.stringify({"fields":{"project":{"id":projectID},
                                    "issuetype":{"name":storyType},
                                    "summary":thisSummary,
                                    "description":"Auto generated",
                                    "customfield_10013":epicLink,
                                    "fixVersions":[{"name":ViewModels.dataVM.fixVersion()}]
                                    }
                        })
        }
        var uri = ViewModels.commonVM.baseIssueEditURI().replace("/{issueIdOrKey}","");
        var request = {
            url : uri,
            type : method,
            contentType: 'application/json',
            data: data,
            success: function(response){
                //console.log(response);
                ViewModels.storyVM.loadStory(epicLink, '', 0);
            },
            error: function(response) {
                console.log(response);
            }
        }
        $.ajax(request);
    }

}