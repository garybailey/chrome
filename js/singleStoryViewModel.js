var singleStoryViewModel = function(item) {
	var self = this;

	self.epicKey = ko.observable(item.fields.customfield_10013);
	self.issueType = ko.observable(item.fields.issuetype.name);
	self.assignee = ko.observable(item.fields.assignee);
	self.fixVersions = ko.observable(item.fields.fixVersions);
	self.status = ko.observable(item.fields.status);
	self.summary = ko.observable(item.fields.summary);
	self.key = ko.observable(item.key);
	self.id = ko.observable(item.id);
	self.sprints = ko.observableArray(item.fields.customfield_10016);
	self.transitionTypes = ko.observableArray([self.status()]);
	
	self.buildNumber = ko.computed(function() {
		if (self.fixVersions().length < 1) return "";
		if (item.fields.customfield_10068 == null ) {
			return (self.fixVersions()[0].name);
		} else {
			return (self.fixVersions()[0].name + "." + item.fields.customfield_10068);
		}
	})

	self.activeSprint = ko.computed(function() {
		var name = new Array([]);
		var fields = new Array([]);
		var tempSprint = new Array([]);
		var tempSprint = ko.utils.arrayFilter(self.sprints(), function(sprint) {
			temp = sprint.split(',');
			return (temp[7].indexOf('completeDate=<null>') > -1)
		})
		if (tempSprint.length == 0) {
			return ("");
		} else {
			fields = tempSprint[0].split(',');
			//console.log(fields);
			name = fields[3].split('=');
			//console.log(name);
			return name[1];
		}
	})
	self.timeEst = ko.computed(function() {
		if (item.fields.aggregatetimeestimate == null) {
			return (0);
		} else {
			return (Math.round(item.fields.aggregatetimeestimate / 3600));
		}
	})
	self.timeOrigEst = ko.computed(function() {
		if (item.fields.aggregatetimeoriginalestimate == null) {
			return (0);
		} else {
			return (Math.round(item.fields.aggregatetimeoriginalestimate / 3600));
		}
	});
	self.timeSpent = ko.computed(function() {
		if (item.fields.aggregatetimespent == null) {
			return (0);
		} else {
			return (Math.round(item.fields.aggregatetimespent / 3600));
		}
	})
	self.timeDelta = ko.computed(function() {
		if (self.timeSpent() == 0) {
			return (0);
		}
		var timeDelta = self.timeOrigEst() - (self.timeSpent() + self.timeEst());
		return (timeDelta);
	})

	self.timeLeft = ko.computed(function() {
		var timeLeft = self.timeOrigEst() - self.timeSpent();
		return (timeLeft);
	})

	self.getAssignee = ko.computed(function() {
		if (self.assignee() !== null) {
			return (self.assignee().displayName);
		} else {
			return ('Unassigned');
		}
	})

	self.updateAvailableTransitions = function() {
		self.transitionTypes([]);
		var uri = ViewModels.commonVM.getTransitions().replace("{issueIdOrKey}",self.key());
		var method = "GET";
		var request = {
			url: uri,
			type: method,
			success: function(data) {
				ko.utils.arrayForEach(data.transitions, function(status) {
					if (status.name !== self.status().name) {
						self.transitionTypes.push(status);
					}
				})
				console.log(self.transitionTypes());
			}
		}
		$.ajax(request);

	}
	self.updateStatus = function(transition) {
		var uri = ViewModels.commonVM.getTransitions().replace("{issueIdOrKey}",self.key());
		var method = "POST";
		var data = JSON.stringify({
						"transition": {
							"id": transition.id
						}
					})
		self.status(transition);
		var request = {
			url: uri,
			type: method,
            contentType: 'application/json',
			data: data,
			success: function(response) {
				//console.log(response);
			},
			error: function(response) {
				console.log(response);
			}
		}
		$.ajax(request);
	}
}